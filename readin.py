import glob
import numpy



def read_structure(filepath='.'):
    '''
    Reads struture files. Set filepath equal to the location of the file
    '''
    files = glob.glob(filepath + 'structure*')
    files = sorted(files)
    namearr = ['m', 'r','l', 'P','rho', 'T','u_e', 's','c_P','Gamma_1','nabla_ad', 'mu','n_e', 'P_e', 'P_rad', 'nabla_rad', 'nabla', 'v_c', 'kappa', 'epsilon_nuc', 'epsilon_pp','epsilon_CNO', 'epsilon_3alpha', 'epsilon_nu_nuc', 'epsilon_nu','epsilon_grav','X_H', 'X_H2', 'X_Hp', 'X_He', 'X_Hep','X_Hepp','X_C','X_N', 'X_O','psi']
    big_dict = {nam:[] for nam in namearr}
    for fil in files:
        datarr = numpy.loadtxt(fil)
        for i in range(len(namearr)):
            big_dict[namearr[i]].append(datarr[:, i])
    return big_dict


def read_summary(filepath ='.'):
    namearr = ['i', 't','M', 'L', 'R',  'T_s',  'T_c','rho_c','P_c','Psi_c', 'X_c','Y_c', 'X_Cc','X_Nc','X_Oc', 't_dyn', 't_KH', 't_nuc',  'L_PP','L_CNO', 'L_3a', 'L_Z','L_nu',  'M_He', 'M_C','M_O', 'R_He',  'R_C', 'R_O']
    dic = {nam:[] for nam in namearr}
    fil = filepath + 'summary.txt'
    datarr = numpy.loadtxt(fil)
    for i, name in enumerate(namearr):
        dic[name] = datarr[:, i]
    return dic
    
