import numpy
import matplotlib.pyplot as plot
import readin


def int_mom(filepath = 'halfsolar/'):
    '''
    Integrates the moment of inertia
    '''
    structure_dict = readin.read_structure(filepath) 
    
    # For this we only need the structure of the last time step (3Gyr)
    
    mr = structure_dict['m'][-1]
    r = structure_dict['r'][-1]
    
    plot.plot(r, mr)
    plot.ylabel('Mass(r) (M$_\odot$)')
    plot.xlabel('Radius (R$_\odot$)')
    plot.show()



def theta(n):
    '''
    Gives the polytropic temperature given the squiggle radius
    '''
    if n == 0:
        theta_fun = lambda squig: 1. - squig**2. / 6
    if n == 1:
        theta_fun = lambda squig: numpy.sin(squig) / squig
    if n == 5:
        theta_fun = lambda squig (1. + squig**2. / 3)**(-.5)
    return theta_fun
