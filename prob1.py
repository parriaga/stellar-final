import matplotlib.pyplot as plot
import numpy
import readin

def conv_vels_solar():
    '''
    Shows convective velocities as a function of radius
    '''
    rarrfive, vmaxfive = get_convs('solar/')
    rarrone, vmaxone = get_convs('solar/')
    derp = readin.read_summary('solar/')
    timeaxis = derp['t']
    plot.semilog(timeaxis, rarrfive)
    plot.plot(rarrone)
    plot.show()
    plot.loglog(timeaxis, vmaxfive)
    plot.plot(vmaxone)
    plot.show()



def get_convs(filepath):
    dic = readin.read_structure(filepath)
    vcs = dic['v_c']
    rs = dic['r']
    vmax = []
    rarr = []
    for r, vc in zip(rs, vcs): 
        rarr.append(r[numpy.where(r == max(r))])
        vmax.append(numpy.max(vc))
    return rarr, vmax
